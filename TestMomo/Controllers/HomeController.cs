﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Web;
using TestMomo.Models;
using Controller = Microsoft.AspNetCore.Mvc.Controller;
using JsonResult = Microsoft.AspNetCore.Mvc.JsonResult;

namespace TestMomo.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _config;


        public HomeController(ILogger<HomeController> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult PayMomo()
        {
            long sum = 700000;
            var endpoint = _config.GetSection("Logging").GetValue<string>("endpoint");
            var partnerCode = _config.GetSection("Logging").GetValue<string>("partnercode");
            var accessKey = _config.GetSection("Logging").GetValue<string>("accesskey");
            var secretKey = _config.GetSection("Logging").GetValue<string>("secretKey");
            var orderInfo = "Ord" + DateTime.Now.ToString("yyyyMMddHHmmss");
            var returnUrl = "https://localhost:44324/Home/ReturnUrl";
            var notifyUrl = "https://localhost:44324/Home/NotifyUrl";

            string amount = sum.ToString();
            string orderId = Guid.NewGuid().ToString();
            string requestId = Guid.NewGuid().ToString();
            string extraData = "";

            //singature

            string rawHash =
                "partnerCode=" + partnerCode +
                "&accessKey=" + accessKey +
                "&requestId=" + requestId +
                "&amount=" + amount +
                "&orderId=" + orderId +
                "&orderInfo=" + orderInfo +
                "&returnUrl=" + returnUrl +
                "&notifyUrl=" + notifyUrl +
                "&extraData=" + extraData;

            MomoSecurity crypto = new MomoSecurity();
            string signature = crypto.signSHA256(rawHash, secretKey);
            JObject message = new JObject
            {
                {"partnerCode",partnerCode },
                {"accessKey",accessKey },
                {"requestId",requestId },
                {"amount", amount },
                {"orderId", orderId},
                {"orderInfo", orderInfo},
                {"returnUrl", returnUrl},
                {"notifyUrl",notifyUrl },
                {"requestType", "captureMoMoWallet"},
                {"signature", signature},
            };
            string response = PaymentRequest.sendPaymentRequest(endpoint, message.ToString());
            JObject jmessage = JObject.Parse(response);
            var url = jmessage.GetValue("payUrl").ToString();
            return Json(url);
        }

        //return page to annouce to user result 

        public IActionResult ReturnUrl()
        {
            var param = Request.QueryString.ToString().Substring(0, Request.QueryString.ToString().IndexOf("signature") - 1);
            param = HttpUtility.UrlDecode(param);
            var momoSignature = Request.QueryString.ToString().Substring(Request.QueryString.ToString().IndexOf("signature") - 1, Request.QueryString.ToString().Length - 1);
            string seretKey = ConfigurationManager.AppSettings["secretKey"].ToString();
            MomoSecurity crypto = new MomoSecurity();
            string signatrue = crypto.signSHA256(param, seretKey);
            if (signatrue != momoSignature)
            {
                ViewBag.message = "Error";
                    return View();
            }
            else
            {
                ViewBag.message = "Success";
            }
            return View();
        }


        //run to update order status
        public JsonResult NotifyUrl()
        {
            string param = "partner_code=" + Request.Headers["partner_code"] + 
                           "&access_key="  + Request.Headers["access_key"] +
                           "&amount=" + Request.Headers["amout"] +
                           "&order_id=" + Request.Headers["order_id"] +
                           "&order_info=" + Request.Headers["order_info"] +
                           "&order_type=" + Request.Headers["order_type"] +
                           "&transaction_id=" + Request.Headers["transaction_id"] +
                           "&message=" + Request.Headers["message"] +
                           "&response_time=" + Request.Headers["response_time"] +
                           "&status_code=" + Request.Headers["status_code"];
            param = HttpUtility.UrlDecode(param);
            MomoSecurity crypto = new MomoSecurity();
            string secretkey = ConfigurationManager.AppSettings["serectkey"].ToString();
            string signatureVerify = crypto.signSHA256(param, secretkey);
            string status_code = Request.Headers["status_code"].ToString();
            var momoSignature = Request.QueryString.ToString().Substring(Request.QueryString.ToString().IndexOf("signature") - 1, Request.QueryString.ToString().Length - 1);
            if (signatureVerify != momoSignature)
            {
                if (status_code != "0")
                {
                    //failed - update Order status
                }
                else
                {
                    //success - update order status
                }
            }else
            {
                //failed
            }
            
            return Json("");
        }
    }
}
